# STM32-429FI, HC-SR04, and WS2812 Radial LED Implementation

![Project Image](https://gitlab.com/enf3tri/radial_led_sr04/-/raw/main/images/Radial_HCSR04_bb.png?ref_type=heads)

This repository provides an example implementation of a system that combines the STM32-429FI microcontroller, the HC-SR04 ultrasonic sensor, and WS2812 radial LEDs to create a fun and interactive project. This README will guide you through setting up and running the project.

## Table of Contents

- [Introduction](#introduction)
- [Hardware Requirements](#hardware-requirements)
- [Software Requirements](#software-requirements)
- [Setup](#setup)
  - [Wiring](#wiring)
  - [Development Environment](#development-environment)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Introduction

The STM32-429FI is a powerful microcontroller with plenty of I/O options, making it ideal for various projects. This project demonstrates the integration of the HC-SR04 ultrasonic sensor and WS2812 radial LEDs with the STM32-429FI to create a radial LED display that responds to the distance measured by the HC-SR04 sensor.

When an object is detected by the HC-SR04 sensor, the STM32-429FI will calculate the distance to the object and display a corresponding pattern on the WS2812 radial LEDs. 

## Hardware Requirements

To replicate this project, you will need the following hardware components:

- STM32-429FI development board
- HC-SR04 ultrasonic sensor
- WS2812 radial LED ring
- Jumper wires
- Power supply for the STM32-429FI

## Software Requirements

You will need the following software tools:

- STM32CubeIDE for developing and flashing the firmware to the STM32-429FI.
- Appropriate device drivers for the STM32-429FI.
- Serial communication software for debugging.

## Setup

### Wiring

Connect the hardware components as follows:

- **HC-SR04 Ultrasonic Sensor**:
  - VCC to 5V on STM32-429FI.
  - GND to GND on STM32-429FI.
  - Trig to a PA9 pin on STM32-429FI.
  - Echo to a PA8 pin on STM32-429FI.

- **WS2812 Radial LED Ring**:
  - Connect data input to a PE9 pin on STM32-429FI.
  - Connect VCC to 5V on STM32-429FI.
  - Connect GND to GND on STM32-429FI.

- **STM32-429FI**:
  - Ensure the board is powered appropriately.

### Development Environment

1. Clone this repository to your development machine.

```bash
git clone https://gitlab.com/enf3tri/radial_led_sr04.git
```
Open the project in STM32CubeIDE.

Configure your project settings, including target microcontroller and debugging tools.

Build and flash the firmware to your STM32-429FI development board.

## Usage
After flashing the firmware onto your STM32-429FI, power it up. The HC-SR04 sensor will continuously measure distances, and the WS2812 radial LEDs will display patterns corresponding to the distance measurements.

You can customize the LED patterns and distance thresholds in the firmware code to create different visual effects based on the detected object's distance.

